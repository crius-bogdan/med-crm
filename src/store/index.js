import Vue from 'vue'
import Vuex from 'vuex'
import * as user from './module/user.js'
import * as worker from './module/worker.js'
import * as manager from './module/manager.js'
import * as invoice from './module/invoice.js'
import Api from "../service/Api";

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    rate_types: null,
    loading: false,

      // [
      //   {
      //     type: 1,
      //     text: 'Werkdag - 8:00 uur tot 20:00 uur',
      //   },
      //   {
      //     type: 2,
      //     text: 'Werkdag - 20:00 tot 08:00 uur',
      //   },
      //   {
      //     type: 3,
      //     text: 'Weekend - 8:00 tot 20:00 uur',
      //   },
      //   {
      //     type: 4,
      //     text: 'Weekend - 20:00 tot 08:00 uur',
      //   },
      //   {
      //     type: 5,
      //     text: 'Vakantie - 8:00 tot 20:00 uur',
      //   },
      //   {
      //     type: 6,
      //     text: 'Vakantie - 20:00 tot 08:00 uur',
      //   },
      // ]
  },
  mutations: {
    SET_RATE_TYPES(state, data) {
      state.rate_types = data;
    },
    SET_LOADING(state, data) {
      state.loading = data;
    }
  },
  actions: {
    get_shifts({ commit }) {
      return Api.get_shifts()
        .then(({ data }) => {
          commit('SET_RATE_TYPES', data.shifts);
        })
    },
    get_shifts_by_company({ commit }, id) {
      return Api.get_shifts_company(id)
        .then(({ data }) => {
          commit('SET_RATE_TYPES', data.shifts);
        })
    },
    set_load({ commit }, data) {
      commit('SET_LOADING', data)
    }
  },
  getters: {
    rate_types: state => state.rate_types ? state.rate_types : null,
    loading: state => state.loading
  },
  modules: {
    user,
    worker,
    manager,
    invoice
  }
})
