import Api from '@/service/Api.js'
export const namespaced = true;
export const state = {
  invoice: null
}
export const getters = {
  invoice: state => {
    return state.invoice ? state.invoice : null
  },
}

export const mutations = {
  SET_INVOICE(state, data) {
    state.invoice = data
  },
  CLEAR_INVOICE(state) {
    state.invoice = null
  }
}

export const actions = {
  get_invoice({ commit }, id) {
    return Api.get_invoice(id)
      .then(({ data: { invoice } }) => {
        commit('SET_INVOICE', invoice)
      })
  }
}
