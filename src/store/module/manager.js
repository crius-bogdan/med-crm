import Api from '@/service/Api.js'
export const namespaced = true;
export const state = {
  hours_worked: null,
  earnings: null,
  rates: null,
  table_working_hours: null,
  total_hours: null,
  manager_table: null,
  notifications: null,
  analytics: null
}
export const getters = {
  hours_worked: state => {
    return state.hours_worked ? state.hours_worked.per_shift : null
  },
  earnings: state => {
    return state.earnings ? state.earnings.shifts : null
  },
  earnings_total: state => {
    return state.earnings ? state.earnings.total : null
  },
  rates: state => {
    return state.rates ? state.rates : null
  },
  table_working_hours: state => {
    return state.table_working_hours ? state.table_working_hours : null
  },
  total_hours_per_shift: state => {
    return state.total_hours ? state.total_hours.total_working_hours_per_shift : null
  },
  total_hours_worked: state => {
    return state.total_hours ? state.total_hours.total_hours_worked : null
  },
  total_to_pay: state => {
    return state.total_hours ? state.total_hours.total_to_pay : null
  },
  manager_table: state => {
    return state.manager_table ? state.manager_table : []
  },
  notifications_count: state => {
    return state.notifications ? state.notifications.length : null
  },
  notifications: state => {
    return state.notifications ? state.notifications : null
  },
  analytics: state => {
    return state.analytics ? state.analytics : null
  },
}

export const mutations = {
  SET_WORKED_HOURS(state, data) {
    state.hours_worked = data;
  },
  SET_EARNINGS(state, data) {
    state.earnings = data;
  },
  SET_RATE(state, data) {
    state.rates = data;
  },
  SET_TABLE_DATA(state, data) {
    state.table_working_hours = data;
  },
  SET_TOTAL_HOURS(state, data) {
    state.total_hours = data;
  },
  SET_COMPANY_TABLE(state, data) {
    state.manager_table = data;
  },
  SET_NOTIFICATIONS(state, data) {
    state.notifications = data;
  },
  DELETE_NOTIFICATION(state, id) {
    const notification_index = state.notifications.findIndex(notification => notification.id === id);
    state.notifications.splice(notification_index, 1);
  },
  SET_ANALYTICS(state, data) {
    state.analytics = data
  },
}

export const actions = {
  update_date({ commit, dispatch }, { id, date }) {
    dispatch('set_load', true, { root: true })
    Api.get_company_data_by_date(id, date)
      .then(({ data: { total_per_shift: { hours_worked, earnings }, rates, working_hours } }) => {
        commit('SET_WORKED_HOURS', hours_worked)
        commit('SET_EARNINGS', earnings)
        commit('SET_RATE', rates)
        commit('SET_TABLE_DATA', working_hours)
        Api.get_total_manager_worked_hours_by_date(id, date)
          .then(({ data: { total_hours } }) => {
            commit('SET_TOTAL_HOURS', total_hours)
            Api.get_company_table_by_date(id, date).then(({ data }) => {
              commit('SET_COMPANY_TABLE', data);
              console.log(data)
              dispatch('set_load', false, { root: true });
            })
          })
      })
  },
  update_analytics({ commit, dispatch }, params) {
    dispatch('set_load', true, { root: true })
    Api.get_analytics_table_by_date(params)
      .then(({ data: { data } }) => {
        commit('SET_ANALYTICS', data)
        dispatch('set_load', false, { root: true })
      })
  },
  get_dashboard_data({ commit }, id) {
    return Api.get_company_data(id)
      .then(({ data: { total_per_shift: { hours_worked, earnings }, rates, working_hours } }) => {
        commit('SET_WORKED_HOURS', hours_worked)
        commit('SET_EARNINGS', earnings)
        commit('SET_RATE', rates)
        commit('SET_TABLE_DATA', working_hours)
      }).catch(({ error }) => {
        console.log(error)
      })
  },
  get_total_worked_hours({ commit }, id) {
    return Api.get_total_manager_worked_hours(id)
      .then(({ data: { total_hours } }) => {
        // console.log(total_hours)
        commit('SET_TOTAL_HOURS', total_hours)
        // console.log(total_working_hours_per_shift)
        // console.log(total_hours_worked)
        // console.log(total_to_pay)
      })
  },
  get_company_table({ commit }, id) {
    return Api.get_company_table(id)
      .then(({ data }) => {
        commit('SET_COMPANY_TABLE', data)
      })
      .catch((data) => {
        console.log(data)
      })
  },
  request_notifications({ commit }) {
    return Api.request_manager_notifications()
      .then(({ data: { data } }) => {
        commit('SET_NOTIFICATIONS', data)
      })
      .catch(({ response }) => {
        console.log(response)
      })
  },
  delete_notification({ commit }, id) {
    commit('DELETE_NOTIFICATION', id)
  },
  get_analytics({ commit }) {
    return Api.get_analytics_table()
      .then(({ data: { data } }) => {
        console.log(data)
        commit('SET_ANALYTICS', data)
      })
      .catch(({ response }) => {
        console.log(response)
      })
  },
  get_totals({ commit }) {
    return Api.get_totals().then((res) => console.log(res))
  }
}
