import Api from '@/service/Api.js'
export const namespaced = true;
export const state = {
  hours_worked: null,
  earnings: {
    shifts: [
      {
        shift_id: 1,
        shift_total_money: 0
      },
      {
        shift_id: 2,
        shift_total_money: 0
      },
      {
        shift_id: 3,
        shift_total_money: 0
      },
      {
        shift_id: 4,
        shift_total_money: 0
      }
    ],
    total: 0
  },
  rates: null,
  table_working_hours: [
    {
      date: '2020-05-01',
      distance: 0,
      note: null,
      formatted_date: 'FR, 05/01',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-02',
      distance: 0,
      note: null,
      formatted_date: 'SA, 05/02',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-03',
      distance: 0,
      note: null,
      formatted_date: 'SU, 05/03',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-04',
      distance: 0,
      note: null,
      formatted_date: 'MO, 05/04',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-05',
      distance: 0,
      note: null,
      formatted_date: 'TU, 05/05',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-06',
      distance: 0,
      note: null,
      formatted_date: 'WE, 05/06',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-07',
      distance: 0,
      note: null,
      formatted_date: 'TH, 05/07',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-08',
      distance: 0,
      note: null,
      formatted_date: 'FR, 05/08',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-09',
      distance: 0,
      note: null,
      formatted_date: 'SA, 05/09',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-10',
      distance: 0,
      note: null,
      formatted_date: 'SU, 05/10',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-11',
      distance: 0,
      note: null,
      formatted_date: 'MO, 05/11',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-12',
      distance: 0,
      note: null,
      formatted_date: 'TU, 05/12',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-13',
      distance: 0,
      note: null,
      formatted_date: 'WE, 05/13',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-14',
      distance: 0,
      note: null,
      formatted_date: 'TH, 05/14',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-15',
      distance: 0,
      note: null,
      formatted_date: 'FR, 05/15',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-16',
      distance: 0,
      note: null,
      formatted_date: 'SA, 05/16',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-17',
      distance: 0,
      note: null,
      formatted_date: 'SU, 05/17',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-18',
      distance: 0,
      note: null,
      formatted_date: 'MO, 05/18',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-19',
      distance: 0,
      note: null,
      formatted_date: 'TU, 05/19',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-20',
      distance: 0,
      note: null,
      formatted_date: 'WE, 05/20',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-21',
      distance: 0,
      note: null,
      formatted_date: 'TH, 05/21',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-22',
      distance: 0,
      note: null,
      formatted_date: 'FR, 05/22',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-23',
      distance: 0,
      note: null,
      formatted_date: 'SA, 05/23',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-24',
      distance: 0,
      note: null,
      formatted_date: 'SU, 05/24',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-25',
      distance: 0,
      note: null,
      formatted_date: 'MO, 05/25',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-26',
      distance: 0,
      note: null,
      formatted_date: 'TU, 05/26',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-27',
      distance: 0,
      note: null,
      formatted_date: 'WE, 05/27',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-28',
      distance: 0,
      note: null,
      formatted_date: 'TH, 05/28',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-29',
      distance: 0,
      note: null,
      formatted_date: 'FR, 05/29',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-30',
      distance: 0,
      note: null,
      formatted_date: 'SA, 05/30',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    },
    {
      date: '2020-05-31',
      distance: 0,
      note: null,
      formatted_date: 'SU, 05/31',
      shifts: [
        {
          shift_id: 1,
          time: '00:00'
        },
        {
          shift_id: 2,
          time: '00:00'
        },
        {
          shift_id: 3,
          time: '00:00'
        },
        {
          shift_id: 4,
          time: '00:00'
        }
      ]
    }
  ]
}
export const getters = {
  hours_worked: state => {
    return state.hours_worked ? state.hours_worked.per_shift : null
  },
  earnings: state => {
    return state.earnings ? state.earnings.shifts : null
  },
  earnings_total: state => {
    return state.earnings ? state.earnings.total : null
  },
  rates: state => {
    return state.rates ? state.rates : null
  },
  table_working_hours: state => {
    return state.table_working_hours ? state.table_working_hours : null
  },
}

export const mutations = {
  SET_WORKED_HOURS(state, data) {
    state.hours_worked = data;
  },
  SET_EARNINGS(state, data) {
    state.earnings = data;
  },
  SET_RATE(state, data) {
    state.rates = data;
  },
  SET_TABLE_DATA(state, data) {
    state.table_working_hours = data;
  },
}

export const actions = {
  get_dashboard_data({ commit }, id) {
    return Api.get_company_data(id)
      .then(({ data: { total_per_shift: { hours_worked, earnings }, rates, working_hours } }) => {
        commit('SET_WORKED_HOURS', hours_worked)
        commit('SET_EARNINGS', earnings)
        commit('SET_RATE', rates)
        commit('SET_TABLE_DATA', working_hours)
        // console.log(hours_worked)
        // console.log(earnings)
        // console.log(rates)
        // console.log(working_hours)
      })
  },
  get_dashboard_data_by_date({ commit }, { id, date }) {
    return Api.get_company_data_by_date_worker(id, date)
      .then(({ data: { total_per_shift: { hours_worked, earnings }, rates, working_hours } }) => {
        commit('SET_WORKED_HOURS', hours_worked)
        commit('SET_EARNINGS', earnings)
        commit('SET_RATE', rates)
        commit('SET_TABLE_DATA', working_hours)
        // console.log(hours_worked)
        // console.log(earnings)
        // console.log(rates)
        // console.log(working_hours)
      })
  }
}
