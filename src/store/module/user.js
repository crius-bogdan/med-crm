import Api from '@/service/Api.js'
export const namespaced = true;
export const state = {
  user: null,
// {
//   name: 'S.Albers',
//     photo: 'img/user.jpg',
//   user_companies: [
//   {
//     id: 234,
//     name: 'Bedrijf 1',
//
//   },
//   {
//     id: 45,
//     name: 'Bedrijf 2',
//   },
//   {
//     id: 456,
//     name: 'Bedrijf 3',
//   }
// ],
//   role: {
//   // manager
//   name: 'manager'
// }
// }
}
export const getters = {
  user_role: state => {
    return state.user ? state.user.has_management_access : null
  },
  user_companies: state => {
    return state.user ? state.user.companies : null
  },
  user_info: state => {
    if(state.user) {
      return {
        name: state.user.name ? state.user.name : null,
        photo: state.user.photo ? state.user.photo : null
      }
    }
    return null
  },
  user_state: state => {
    return state.user
  },
}

export const mutations = {
  CHANGE_ROLE(state, data) {
    state.user.role.name = data
  },
  SET_USER_DATA(state, user) {
    state.user = user
  }
}

export const actions = {
  change_role({commit}, data) {
    commit('CHANGE_ROLE', data)
  },
  get_user({ commit }) {
    return new Promise((resolve, reject) => {
      Api.get_user()
        .then(({ data: { user } }) => {
          commit('SET_USER_DATA', user)
          // console.log(user)
          resolve()
        })
        .catch(error => console.log(error))
    })
  },
  logout() {
    return Api.log_out()
  }
}
