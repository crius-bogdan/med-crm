import axios from 'axios'
import moment from "moment";

const $http = axios;
$http.defaults.baseURL = "https://zzp.zorgkoppeling.nl/";
// $http.defaults.baseURL = "http://3.17.174.41/";
$http.defaults.headers["Content-Type"] = "application/json;charset=UTF-8";
$http.defaults.headers.common["Accept"] = "application/json";
// $http.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
// headers: {
//   'X-Requested-With': 'XMLHttpRequest'
//   // 'X-CSRF-Token': store.getters.appConfig('csrf')
// }
// xsrfCookieName: "XSRF-TOKEN"
// xsrfHeaderName: "X-XSRF-TOKEN"
const current_date = () => {
  const start_date = moment().startOf('month').format('YYYY-MM-DD')
  const end_date = moment().endOf('month').format('YYYY-MM-DD')
  return `start_date=${start_date}&end_date=${end_date}`
}
const data_obj_to_string = (obj) => {
  let string = '';
  for (let k in obj) {
    string += `${k}=${obj[k]}&`;
  }
  return string
}
export default {
  get_user() {
    return $http.get('/user')
  },
  get_shifts() {
    return $http.get('/shifts')
  },
  get_shifts_company(company) {
    return $http.get('/shifts?company_id=' + company)
  },
  get_company_data(company_id) {
    return $http.get(`/company/${company_id}?${current_date()}`)
  },
  get_company_data_by_date_worker(company_id, date) {
    return $http.get(`/company/${company_id}?${data_obj_to_string(date)}`)
  },
  get_total_manager_worked_hours(company_id) {
    return $http.get(`/manager/company/total-hours/${company_id}?${current_date()}`)
  },
  get_company_table(company_id) {
    return $http.get(`/manager/company/users/${company_id}?${current_date()}`)
  },
  get_company_table_by_date(company_id, date) {
    console.log(date)
    return $http.get(`/manager/company/users/${company_id}?${data_obj_to_string(date)}`)
  },
  request_to_change_shifts(data) {
    return $http.post('/request-change', data, {
      headers: {
        'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
      }
    })
  },
  request_manager_notifications() {
    return $http.get(`/manager/requested-changes?${current_date()}`)
  },
  manager_update_request_changes(data) {
    return $http.post('/manager/update-req-changes-status', data, {
      headers: {
        'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').getAttribute('content')
      }
    })
  },
  get_analytics_table() {
    return $http.get(`/manager/user-totals?${current_date()}`)
  },
  get_company_data_by_date(company_id, params) {
    return $http.get(`/company/${company_id}?${data_obj_to_string(params)}`)
  },
  get_total_manager_worked_hours_by_date(company_id, params) {
    return $http.get(`/manager/company/total-hours/${company_id}?${data_obj_to_string(params)}`)
  },
  get_analytics_table_by_date(params) {
    return $http.get(`/manager/user-totals?${data_obj_to_string(params)}`)
  },
  log_out() {
    return $http.get('/user/logout')
  },
  get_invoice(id) {
    return $http.get('/invoice/' + id)
  },
  get_totals() {
    return $http.get('/user-totals')
  },
}
