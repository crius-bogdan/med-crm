import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import upperFirst from 'lodash/upperFirst'
import camelCase from 'lodash/camelCase'
import "./directives";

const requireComponent = require.context(
  './components/Base',
  false,
  /Base[A-Z]\w+\.(vue|js)$/
)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName)

  const componentName = upperFirst(
    camelCase(fileName.replace(/^\.\/(.*)\.\w+$/, '$1'))
  )

  Vue.component(componentName, componentConfig.default || componentConfig)
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  created() {
    store.dispatch('user/get_user')
      .then(() => {
        store.dispatch('get_shifts')
          .then(() => {
            const DashboardView = store.getters['user/user_role'] ? 'ManagerDashboard' : 'WorkerDashboard';
            // console.log(DashboardView)
            // console.log()
            router.push({ name: DashboardView, params: { id: store.getters['user/user_companies'][0].id } })
            // next()
          })
          .catch(() => {
            // console.log('get_shifts')
          })
      })
      .catch(() => {
        // console.log('user/get_user')
      })
  },
  render: h => h(App)
}).$mount('#app')
