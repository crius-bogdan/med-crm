import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store';
Vue.use(VueRouter)
const user_auth = () => {
  return store.getters['user/user_state']
}
const is_empty_manager_notifications = () => {
  return store.getters['manager/notifications']
}
const routes = [
  {
    path: '/',
    name: 'index',
    component: () => import('@/views/Index'),
    meta: {
      layout: 'Empty'
    },
    // beforeEnter(from, to, next) {
    //
    // }
  },
  {
    path: '/manager/company/:id',
    name: 'ManagerDashboard',
    component: () => import('@/views/ManagerDashboard'),
    beforeEnter(from, to, next) {
      // if(user_auth) {
      //   next({ name: 'index' })
      // }
      // console.log(document.querySelector('meta[name="viewport"]'))
      store.dispatch('set_load', true)
      store.dispatch('manager/get_dashboard_data', from.params.id)
        .then(() => {
          store.dispatch('manager/get_total_worked_hours', from.params.id)
            .then(() => {
              store.dispatch('manager/request_notifications')
                .then(() => {
                  store.dispatch('get_shifts_by_company', from.params.id)
                    .then(() => {
                      next();
                      store.dispatch('set_load', false);
                    })
                })
                .catch(() => {
                  next({name: 'index'})
                })
              // store.dispatch('manager/get_company_table', from.params.id)
              //   .then(() => {
              //
              //   })
              //   .catch(() => {
              //     next({name: 'index'})
              //   })
            })
            .catch(() => {
              console.log('get_total_worked_hours')
              next({name: 'index'})
            })
        })
        .catch(() => {
          console.log('get_dashboard_data')
        next({name: 'index'})
      })
    }
  },
  {
    path: '/worker/company/:id',
    name: 'WorkerDashboard',
    props: true,
    component: () => import('@/views/WorkerDashboard'),
    beforeEnter(from, to, next) {
      if(user_auth) {
        next({ name: 'index' })
      }
      store.dispatch('set_load', true)
      store.dispatch('worker/get_dashboard_data', from.params.id)
        .then(() => {
          store.dispatch('get_shifts_by_company', from.params.id)
            .then(() => {
              next();
              store.dispatch('set_load', false);
            })
        })
        .catch(() => {
          next({ name: 'index' })
        })
    },
  },
  {
    path: '/analytics',
    name: 'Analytics',
    component: () => import('@/views/AnalyticsWorkers'),
    beforeEnter(from, to, next) {
      store.dispatch('set_load', true)
      store.dispatch('manager/get_analytics')
        .then(() => {
          store.dispatch('get_shifts', from.params.id)
            .then(() => {
              next();
              store.dispatch('set_load', false);
            })
      }).catch(() => next({ name: 'index' }))
    }
  },
  {
    path: '/notifications',
    name: 'Notifications',
    component: () => import('@/views/Notifications'),
    beforeEnter(from, to, next) {
      store.dispatch('set_load', true);
      if(!is_empty_manager_notifications()) {
          store.dispatch('set_load', false);
          next({ name: 'index' })
          return
      }
      store.dispatch('get_shifts').then(() => {
        store.dispatch('set_load', false);
        next()
      })
    }
  },
  {
    path: '/invoice/:company_id',
    name: 'invoice',
    component: () => import('@/views/Invoice'),
    beforeEnter(from, to, next) {
      store.dispatch('set_load', true)
      if(!from.params.company_id) next(false)
      store.dispatch('invoice/get_invoice', from.params.company_id)
        .then(() => {
          next()
        })
      // next()
      store.dispatch('set_load', false)
    }
  },
  {
    path: '/invoice-history',
    name: 'invoice-history',
    component: () => import('@/views/InvoiceHistory'),
    // beforeEnter(from, to, next) {
    //   console.log(from.params)
    //   store.dispatch('set_load', true)
    //   if(!from.params.company_id) next(false)
    //   store.dispatch('invoice/get_invoice', from.params.company_id)
    //     .then(() => {
    //       next()
    //     })
    //   // next()
    //   store.dispatch('set_load', false)
    // }
  },
]

const router = new VueRouter({
  // mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
