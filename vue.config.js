module.exports = {
  publicPath:
    // process.env.NODE_ENV === 'production'
    //   ? '/' + process.env.CI_PROJECT_NAME + '/'
    //   : '/',
  process.env.NODE_ENV === 'production'
      ? 'themes/demo/assets/crm/'
      : '/',
  css: {
    sourceMap: true,
    loaderOptions: {
      sass: {
        prependData: `@import "~@/assets/scss/_includes.scss";`
      }
    }
  }
};
