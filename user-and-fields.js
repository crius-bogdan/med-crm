user: {
    name: 'S.Albers',
    photo: 'img/user.jpg',
    user_companies: [
    {
      id: 234,
      name: 'Bedrijf 1',

    },
    {
      id: 45,
      name: 'Bedrijf 2',
    },
    {
      id: 456,
      name: 'Bedrijf 3',
    }
  ],
    role: {
    // manager
    name: 'manager'
  }
}

time_periods: [
  {
    id: int (unique),
    name: String,
    time: {
      from: Float,
      to: Float
    }
    ...
  }
],

const table = {
  time_periods: [
    {
      id: int (unique),
      name: String,
      time: {
        from: Float,
        to: Float
      }
        ...
    }
  ],
  table_body: [
    {
      id: int (unique),
      date: 'Formatted Date String',
      distance: int/String
      table_line: [
        {
          id: int (unique),
          period_id: int 'id of time_periods',
          period_val: String
        }
      ]
    }
  ]
}